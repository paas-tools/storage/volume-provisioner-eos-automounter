/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"os"

	volumeprovisioner "gitlab.cern.ch/paas-tools/volume-provisioner-package"
	"k8s.io/api/core/v1"
	storage_v1 "k8s.io/api/storage/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var EOS_HOSTPATH = os.Getenv("EOS_HOSTPATH")
var EOS_VOLUMEPROVISIONER_NAME = os.Getenv("EOS_VOLUMEPROVISIONER_NAME")

// EOSVolumeProvisioner represents a VolumeProvisioner for EOS.
type EOSVolumeProvisioner struct{}

func NewEOSVolumeProvisioner() *EOSVolumeProvisioner {
	return &EOSVolumeProvisioner{}
}

// provisionVolume dynamically creates the volume share for the PVC. For EOS requests
// there is nothing to provisioning so the method just returns `nil`
func (vp *EOSVolumeProvisioner) provisionVolume() error {
	return nil
}

// CreatePV takes care of creating a EOS v1.PersistentVolume using a hostPath to `/eos/`.
func (vp *EOSVolumeProvisioner) CreatePV(pvc *v1.PersistentVolumeClaim, storageClass *storage_v1.StorageClass) (*v1.PersistentVolume, error) {

	// Provision the volume according to the requirements of the v1.PersistentVolumeClaim.
	// For EOS there is nothing to do however, as volumes are already provisioned and
	// mounted in the OpenShift servers
	if err := vp.provisionVolume(); err != nil {
		return nil, err
	}

  hostPathDirectory := v1.HostPathDirectory
	// Create EOS PV and bind it to existing PVC
	pv := &v1.PersistentVolume{
		ObjectMeta: meta_v1.ObjectMeta{
			GenerateName: fmt.Sprintf("pv-%s-", *volumeprovisioner.GetStorageClassName(pvc)),
		},
		Spec: v1.PersistentVolumeSpec{
			Capacity: v1.ResourceList{
				v1.ResourceName(v1.ResourceStorage): pvc.Spec.Resources.Requests[v1.ResourceName(v1.ResourceStorage)],
			},
			// EOS needs to be read only
			AccessModes: []v1.PersistentVolumeAccessMode{v1.ReadOnlyMany, v1.ReadWriteMany, v1.ReadWriteOnce},
			PersistentVolumeSource: v1.PersistentVolumeSource{
				HostPath: &v1.HostPathVolumeSource{
					Path: EOS_HOSTPATH,
					Type: &hostPathDirectory,
				},
			},
			ClaimRef: &v1.ObjectReference{
				Kind:      "PersistentVolumeClaim",
				Name:      pvc.GetName(),
				Namespace: pvc.GetNamespace(),
			},
			PersistentVolumeReclaimPolicy: "Delete",
		},
	}
	// Return the volume. Please note it has not been saved yet so at this point it
	// still does not exist
	return pv, nil
}

// UnprovisionPV removes the physical volume associated with the PV. For EOS volumes
// there is nothing to do so the funcion just returns nil
func (vp *EOSVolumeProvisioner) UnprovisionPV(pv *v1.PersistentVolume) error {
	return nil
}

// GetProvisionerName obtain name of this provisioner. This VolumeProvisioner will only act
// on PVCs whose StorageClass has this provisioner parameter
func (vp *EOSVolumeProvisioner) GetProvisionerName() string {
	return EOS_VOLUMEPROVISIONER_NAME
}
