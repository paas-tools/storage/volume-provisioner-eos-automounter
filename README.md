## Volume provisioner controller for EOS volumes (mounted via EOS Automounter)
This Kubernetes controller takes care of provision EOS PersistentVolumes objects based on incoming PersistentVolumeClaims.

### Deployment
The volume provisioner for EOS automounter is deployed in all OpenShift clusters i.e., `openshift.cern.ch`, `openshift-dev.cern.ch` and `openshift-test.cern.ch` in the `paas-infra-eos` namespace. The deployment is done via the Helm template present [here](https://gitlab.cern.ch/paas-tools/infrastructure/eos-automounter-helm).
